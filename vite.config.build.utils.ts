import { defineConfig } from 'vite'
import path from 'path'
import dts from 'vite-plugin-dts'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [dts()],
  build: {
    minify: true,
    lib: {
      entry: path.resolve(__dirname, './src/index.ts'),
      name: 'name',
      fileName: (format) => `index.${format}.ts`
    },
    rollupOptions: {
      output: {
        dir: path.resolve(__dirname, './dist')
      }
    },
    emptyOutDir: false
  }
})
