# plugin-validate

## plugin-validate 工具类组件
### 安装
```bash
npm i -S plugin-validate
```
### API
```
import { enums, checkNumber } from 'plugin-validate'
 
 checkNumber('123456')
```

### 发布
* 修改package.json中的version版本号
```
 "version": "0.0.1",
```
* npm run build
* npm publish
