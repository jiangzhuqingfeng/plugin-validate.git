//验证为正整数   数字使用type=number即可
const checkNumber = (value: any): boolean => {
    return /^([1-9][0-9]*)$/.test(value)
}
const colors = {
    red: 1,
    green: 2,
    blue: 3,
}
export { checkNumber, colors }