/**
 * 数字用分隔符(,)显示
 */
const moneyDisplay = (v: number): string | null => {
    return (v + '').replace(/(?=((?!\b)\d{3})+$)/g, ',');
}
export {
    moneyDisplay
}